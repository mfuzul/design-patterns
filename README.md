# Design patterns

PHP implementation of the famous design pattern catalogue: [Design Patterns: Elements of Reusable Object-Oriented Software](https://www.amazon.com/Design-Patterns-Elements-Reusable-Object-Oriented/dp/0201633612)

## List

### [Behavioral](https://bitbucket.org/mfuzul/design-patterns/src/master/Behavioral/)

- Chain of responsibility
- Command
- Interpreter
- Iterator
- Mediator
- Memento
- Observer
- State
- Strategy
- Template Method
- Visitor

### [Creational](https://bitbucket.org/mfuzul/design-patterns/src/master/Creational/)

- [Abstract Factory](https://bitbucket.org/mfuzul/design-patterns/src/master/Creational/Object/AbstractFactory/)
- Builder
- Factory Method
- Prototype
- Singleton

### [Structural](https://bitbucket.org/mfuzul/design-patterns/src/master/Structural/)

- Adapter
- Bridge
- Composite
- Decorator
- Facade
- Flyweight
- Proxy