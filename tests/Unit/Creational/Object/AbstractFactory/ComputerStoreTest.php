<?php
declare(strict_types=1);

namespace Tests\DesignPatterns\Unit\Creational\Object\AbstractFactory;

use DesignPatterns\Creational\Object\AbstractFactory\AmdComponentFactory;
use DesignPatterns\Creational\Object\AbstractFactory\ComputerComponentFactoryInterface;
use DesignPatterns\Creational\Object\AbstractFactory\ComputerStore;
use DesignPatterns\Creational\Object\AbstractFactory\IntelComponentFactory;
use DesignPatterns\Creational\Object\AbstractFactory\Unit\AmdCpuUnit;
use DesignPatterns\Creational\Object\AbstractFactory\Unit\AmdGpuUnit;
use DesignPatterns\Creational\Object\AbstractFactory\Unit\IntelCpuUnit;
use DesignPatterns\Creational\Object\AbstractFactory\Unit\IntelGpuUnit;
use PHPUnit\Framework\TestCase;

class ComputerStoreTest extends TestCase
{
    private $computerStore;

    /**
     * @dataProvider componentFactoryProvider
     */
    public function testComputerStoreAssembleForComponentFactory(
        ComputerComponentFactoryInterface $factory,
        string $cpuId,
        float $cpuFrequency,
        string $gpuId,
        int $gpuMemory,
        string $cpuUnitClass,
        string $gpuUnitClass
    ) {
        $this->computerStore = new ComputerStore($factory);

        $this->computerStore->assembleComputer(
            $cpuId,
            $cpuFrequency,
            $gpuId,
            $gpuMemory
        );

        $cpuUnit = $this->computerStore->getCpuUnit();
        $gpuUnit = $this->computerStore->getGpuUnit();

        $this->assertInstanceOf($cpuUnitClass, $cpuUnit);
        $this->assertEquals($cpuId, $cpuUnit->cpuId());
        $this->assertEquals($cpuFrequency, $cpuUnit->cpuFrequency());

        $this->assertInstanceOf($gpuUnitClass, $gpuUnit);
        $this->assertEquals($gpuId, $gpuUnit->gpuId());
        $this->assertEquals($gpuMemory, $gpuUnit->gpuMemory());
    }

    public function componentFactoryProvider(): array
    {
        return [
            [
                new IntelComponentFactory(),
                'i5-7300HQ',
                2.4,
                'Intel Graphics 5000',
                2048,
                IntelCpuUnit::class,
                IntelGpuUnit::class,
            ],
            [
                new AmdComponentFactory(),
                'i5-7300HQ',
                2.4,
                'Intel Graphics 5000',
                2048,
                AmdCpuUnit::class,
                AmdGpuUnit::class,
            ],
        ];
    }
}
