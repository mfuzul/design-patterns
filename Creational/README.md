# Creational patterns

## List

- Abstract Factory
- Builder
- Factory Method
- Prototype
- Singleton

## Notes

- **abstract** the instantiation process
- class creational pattern uses **inheritance** to vary the class that's instantiated
- object creational pattern will **delegate** instantiation to another object
- object composition > class inheritance
- **encapsulate knowledge** about which concrete classes the system uses
- hide how instances of these classes are created and put together
