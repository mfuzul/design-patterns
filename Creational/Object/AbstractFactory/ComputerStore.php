<?php
declare(strict_types=1);

namespace DesignPatterns\Creational\Object\AbstractFactory;

use DesignPatterns\Creational\Object\AbstractFactory\Unit\CpuUnitInterface;
use DesignPatterns\Creational\Object\AbstractFactory\Unit\GpuUnitInterface;

class ComputerStore
{
    private $computerComponentFactory;

    private $cpuUnit;

    private $gpuUnit;

    public function __construct(
        ComputerComponentFactoryInterface $computerComponentFactory
    ) {
        $this->computerComponentFactory = $computerComponentFactory;
    }

    public function assembleComputer(
        string $cpuId,
        float $cpuFrequency,
        string $gpuId,
        int $gpuMemory
    ): void {
        $this->cpuUnit = $this->computerComponentFactory->createCpuUnit(
            $cpuId,
            $cpuFrequency
        );
        $this->gpuUnit = $this->computerComponentFactory->createGpuUnit(
            $gpuId,
            $gpuMemory
        );
    }

    public function getCpuUnit(): CpuUnitInterface
    {
        return $this->cpuUnit;
    }

    public function getGpuUnit(): GpuUnitInterface
    {
        return $this->gpuUnit;
    }
}
