# Abstract Factory

## Intent

- provide an interface for creating families of related or dependent objects without specifying their concrete classes

## Also known as
 
- Kit

## Applicability

- making the system independent of the creation, composition and representation of it's products
- system should supported and be configured by one of multiple families of products
- enforcing a constraint to use a family of products together
- providing a class library of products, revealing only their interfaces (and not implementations)

## Participants

- AbstractFactory
    - interface for operations which create abstract product objects
- ConcreteFactory
    - implements operations to create concrete product objects
- AbstractProduct
    - declares an interface for a type of product object
- ConcreteProduct
    - defines a product object to be created by the corresponding concrete factory
    - implements the AbstractProduct interface
- Client
    - uses only interfaces declared by AbstractFactory and AbstractProduct classes

## Consequences

- isolation of concrete classes
- easy exchange of product families
- consistency among products
- supporting new kinds of products is difficult

## Related patterns

- Factory Methods
- Prototype
- Singleton