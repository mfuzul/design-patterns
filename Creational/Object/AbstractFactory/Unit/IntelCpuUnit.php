<?php
declare(strict_types=1);

namespace DesignPatterns\Creational\Object\AbstractFactory\Unit;

class IntelCpuUnit implements CpuUnitInterface
{
    private $cpuId;

    private $cpuFrequency;

    public function __construct(string $cpuId, float $cpuFrequency)
    {
        $this->cpuId = $cpuId;
        $this->cpuFrequency = $cpuFrequency;
    }

    public function cpuMaker(): string
    {
        return 'Intel';
    }

    public function cpuId(): string
    {
        return $this->cpuId;
    }

    public function cpuFrequency(): float
    {
        return $this->cpuFrequency;
    }
}
