<?php
declare(strict_types=1);

namespace DesignPatterns\Creational\Object\AbstractFactory\Unit;

interface GpuUnitInterface
{
    public function __construct(
        string $gpuId,
        int $gpuMemory
    );

    public function gpuMaker(): string;

    public function gpuId(): string;

    public function gpuMemory(): int;
}
