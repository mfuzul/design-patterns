<?php
declare(strict_types=1);

namespace DesignPatterns\Creational\Object\AbstractFactory\Unit;

class AmdGpuUnit implements GpuUnitInterface
{
    private $gpuId;

    private $gpuMemory;

    public function __construct(string $gpuId, int $gpuMemory)
    {
        $this->gpuId = $gpuId;
        $this->gpuMemory = $gpuMemory;
    }

    public function gpuMaker(): string
    {
        return 'AMD';
    }

    public function gpuId(): string
    {
        return $this->gpuId;
    }

    public function gpuMemory(): int
    {
        return $this->gpuMemory;
    }
}
