<?php
declare(strict_types=1);

namespace DesignPatterns\Creational\Object\AbstractFactory\Unit;

interface CpuUnitInterface
{
    public function __construct(
        string $cpuId,
        float $cpuFrequency
    );

    public function cpuMaker(): string;

    public function cpuId(): string;

    public function cpuFrequency(): float;
}
