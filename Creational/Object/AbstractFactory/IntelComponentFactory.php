<?php
declare(strict_types=1);

namespace DesignPatterns\Creational\Object\AbstractFactory;

use DesignPatterns\Creational\Object\AbstractFactory\Unit\CpuUnitInterface;
use DesignPatterns\Creational\Object\AbstractFactory\Unit\GpuUnitInterface;
use DesignPatterns\Creational\Object\AbstractFactory\Unit\IntelCpuUnit;
use DesignPatterns\Creational\Object\AbstractFactory\Unit\IntelGpuUnit;

class IntelComponentFactory implements ComputerComponentFactoryInterface
{
    public function createCpuUnit(
        string $cpuId,
        float $cpuFrequency
    ): CpuUnitInterface {
        return new IntelCpuUnit(
            $cpuId,
            $cpuFrequency
        );
    }

    public function createGpuUnit(
        string $gpuId,
        int $gpuMemory
    ): GpuUnitInterface {
        return new IntelGpuUnit(
            $gpuId,
            $gpuMemory
        );
    }
}
