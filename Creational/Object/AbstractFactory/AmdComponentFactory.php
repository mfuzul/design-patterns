<?php
declare(strict_types=1);

namespace DesignPatterns\Creational\Object\AbstractFactory;

use DesignPatterns\Creational\Object\AbstractFactory\Unit\CpuUnitInterface;
use DesignPatterns\Creational\Object\AbstractFactory\Unit\GpuUnitInterface;
use DesignPatterns\Creational\Object\AbstractFactory\Unit\AmdCpuUnit;
use DesignPatterns\Creational\Object\AbstractFactory\Unit\AmdGpuUnit;

class AmdComponentFactory implements ComputerComponentFactoryInterface
{
    public function createCpuUnit(
        string $cpuId,
        float $cpuFrequency
    ): CpuUnitInterface {
        return new AmdCpuUnit(
            $cpuId,
            $cpuFrequency
        );
    }

    public function createGpuUnit(
        string $gpuId,
        int $gpuMemory
    ): GpuUnitInterface {
        return new AmdGpuUnit(
            $gpuId,
            $gpuMemory
        );
    }
}
