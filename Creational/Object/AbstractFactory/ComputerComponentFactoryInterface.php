<?php
declare(strict_types=1);

namespace DesignPatterns\Creational\Object\AbstractFactory;

use DesignPatterns\Creational\Object\AbstractFactory\Unit\CpuUnitInterface;
use DesignPatterns\Creational\Object\AbstractFactory\Unit\GpuUnitInterface;

interface ComputerComponentFactoryInterface
{
    public function createCpuUnit(
        string $cpuId,
        float $cpuFrequency
    ): CpuUnitInterface;

    public function createGpuUnit(
        string $gpuId,
        int $gpuMemory
    ): GpuUnitInterface;
}
